/*
 * Copyright <2017> <Jakub Skořepa, Klára Jakešová>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once
#include <stdint.h> // uint8_t
#include <stdio.h> // printf
#include <stdlib.h> // exit, rand
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include "draw.h"
#include "mzapo_phys.h"

// global variables
extern unsigned char *knobs_mem_base;
extern const char *memdev;

#define true 1
#define false 0
/**
 * @brief contains values of knobs
 */
struct knobs {
    uint8_t blue_value;
    uint8_t green_value;
    uint8_t red_value;

    bool blue_pressed;
    bool green_pressed;
    bool red_pressed;

    int8_t blue;
    int8_t green;
    int8_t red;
};

typedef struct knobs knobs;

#define SPILED_REG_BASE_PHYS 0x43c40000
#define SPILED_REG_SIZE      0x00004000

#define SPILED_REG_KNOBS_8BIT_o         0x024

#define PARLCD_REG_BASE_PHYS 0x43c00000
#define PARLCD_REG_SIZE      0x00004000

uint32_t knobs_read();
void knobs_init(knobs *knobs);
void knobs_update(knobs *knobs);
void knobs_update_from_value(knobs *knobs, uint32_t kvalue);
void knobs_draw(Framebuffer& fb, knobs* knobs);
void knobs_init_from_value(knobs *knobs, uint32_t value);
void init();
void sleep_ms(uint32_t ms);
uint64_t time_us();
